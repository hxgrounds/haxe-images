ARG BASE=debian:bookworm-slim
FROM ${BASE} as build

# install dependencies
RUN rm -f /etc/apt/apt.conf.d/docker-clean \
    && apt update \
    && apt install -y --no-install-recommends \
        g++ gcc make \
    && rm -rf /var/lib/apt/lists/*

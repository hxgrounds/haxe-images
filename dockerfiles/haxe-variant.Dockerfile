ARG HAXE_IMG=hxgrounds/lix-haxe-cache:bookworm-slim
ARG BASE=debian:bookworm-slim

FROM ${HAXE_IMG} as haxeimg

# Separate base stage to allow change it at bake file
FROM ${BASE} as base

FROM base as install

# COPY binaries and reinstall packages
#
COPY --from=haxeimg /var/cache/apt/archives/*.deb /var/cache/apt/archives/
COPY --from=haxeimg /var/lib/apt/lists/ /var/lib/apt/lists/
#
# prevent script to calling apt update and installing lix
ENV NO_UPDATE=1
ENV NO_LIX=1
ENV DEBIAN_FRONTEND=noninteractive
RUN --mount=type=bind,source=./scripts,target=/scripts \
    rm -f /etc/apt/apt.conf.d/docker-clean \
    && sh /scripts/install-lix-and-dependencies.sh
#
# Copy lix
#
COPY --from=haxeimg /usr/local/share/.config/yarn/global /usr/local/share/.config/yarn/global
COPY --from=haxeimg /usr/local/bin /usr/local/bin


# Create a default non-root user to be used by lix, so lix files will not be owned by root

ARG LIX_USER=lix-user
ENV LIX_USER=${LIX_USER}
#
ARG HAXE_USER=${LIX_USER}
ENV USER_HOME=/home/${LIX_USER}
#
RUN if ! id "${LIX_USER}" >/dev/null 2>&1; then \
        echo "Creating ${LIX_USER}" \
        && useradd --user-group --create-home --no-log-init ${LIX_USER}; \
    else \
        echo "User ${LIX_USER} already exist!"; \
    fi
WORKDIR ${USER_HOME}

# Copy haxe and neko
#
COPY --from=haxeimg /home/${HAXE_USER} ${USER_HOME}

# Use root to allow installation of packages by users
USER root

# Forces home to be lix home (where haxe was downloaded)
ENV HOME=${USER_HOME}
CMD ["haxe"]

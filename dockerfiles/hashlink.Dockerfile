ARG BUILDER_BASE=debian:bookworm-slim
ARG BASE=debian:bookworm-slim
FROM ${BUILDER_BASE} as build

# install dependencies
ENV lib_dependencies="libpng-dev libturbojpeg0-dev libvorbis-dev libopenal-dev libsdl2-dev libmbedtls-dev libuv1-dev libsqlite3-dev libglut-dev"
RUN rm -f /etc/apt/apt.conf.d/docker-clean \
    && apt update \
    && apt install -y --no-install-recommends \
        g++ make \
        curl ca-certificates \
        ${lib_dependencies}  \
    && rm -rf /var/lib/apt/lists/*

# Setup build directory
ENV build_dir=/hashlink
RUN mkdir -p ${build_dir}
WORKDIR ${build_dir}

# Download and build --------------------
#
ARG hashlink_version=1.13
ENV hashlink_version=${hashlink_version}
ARG SOURCE="https://github.com/HaxeFoundation/hashlink/archive/refs/tags/${hashlink_version}.tar.gz"
ENV SOURCE=${SOURCE}
ENV release_dir=${build_dir}/release
ENV hashlink_source=${build_dir}/hashlink-source
#
RUN curl -L "$SOURCE" -o package.tar.gz \
    && tar -xzvf package.tar.gz \
    && rm package.tar.gz \
#
    && mv hashlink-${hashlink_version} ${hashlink_source}

WORKDIR ${hashlink_source}
RUN make

# create release by installing in local folder
RUN PREFIX=${release_dir} make install
RUN export LIB_DEPS=$(dpkg -L ${lib_dependencies} | grep "\.so") \
    && cp $LIB_DEPS ${release_dir}/lib
#
# Cleanup:
RUN cd ${build_dir} \
    && rm -r ${hashlink_source}

# -------------------------

FROM ${BASE} as runner

COPY --from=build /hashlink/release/ /usr/local/
CMD [ "hl" ]

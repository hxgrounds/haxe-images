ARG BASE=debian:bookworm-slim
#
# Separate base stage to allow change it at bake file
FROM ${BASE} as base

FROM base as install

ARG LIX_VERSION=latest
#
ENV DEBIAN_FRONTEND=noninteractive
#
# Install dependencies from script:
# - lix (from yarn or npm)
# - nodejs (for running lix and yarn)
# - zip, to allow compacting haxelibs
# - git, to enable inclusion of libraries from git
#
ARG KEEP_DOWNLOADS=""
ENV KEEP_DOWNLOADS=${KEEP_DOWNLOADS}
RUN --mount=type=bind,source=./scripts,target=/scripts \
    rm -f /etc/apt/apt.conf.d/docker-clean \
    && sh /scripts/install-lix-and-dependencies.sh

# Create a default non-root user to be used by lix, so lix files will not be owned by root
ARG LIX_USER=lix-user
ENV LIX_USER=${LIX_USER}
RUN if ! id "${LIX_USER}" >/dev/null 2>&1; then \
        useradd --user-group --create-home --no-log-init ${LIX_USER}; \
    fi

WORKDIR /home/${LIX_USER}

# Download and cache haxe and neko
#
ARG HAXE_VERSION=4.3.2
USER ${LIX_USER}
RUN if [ -z "$HAXE_VERSION" ]; then \
        echo "No haxe version specified"; \
    else \
        lix download haxe ${HAXE_VERSION} \
        && lix use haxe ${HAXE_VERSION} \
        && neko -version \
        && haxe -version \
        ;\
    fi

# Use root to allow installation of packages by users
USER root

# Forces home to be lix home (where haxe was downloaded)
ENV HOME=/home/${LIX_USER}
CMD ["haxe"]

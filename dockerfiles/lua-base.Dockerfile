ARG BASE=nickblah/lua:luarocks
FROM ${BASE} as build

ENV build_dependencies="g++ gcc make libssl-dev cmake git libpcre2-dev"
ENV runtime_dependencies="libssl3 libpcre2-posix3"
ENV lua_dependencies="lrexlib-pcre2 luasocket luasec luv luautf8 hx-lua-simdjson bit32"

RUN --mount=type=bind,source=./scripts,target=/scripts \
    sh /scripts/install-packages.sh ${runtime_dependencies} ${build_dependencies} \
    && sh /scripts/luarocks-install.sh ${lua_dependencies} \
    && apt-get purge -y --autoremove ${build_dependencies} \
    && rm -rf /var/lib/apt/lists/*

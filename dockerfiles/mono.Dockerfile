ARG BASE=debian:bookworm-slim
FROM ${BASE} as build

# sudo apt install dirmngr ca-certificates gnupg
# sudo gpg --homedir /tmp --no-default-keyring --keyring /usr/share/keyrings/mono-official-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
# echo "deb [signed-by=/usr/share/keyrings/mono-official-archive-keyring.gpg] https://download.mono-project.com/repo/debian stable-buster main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
# sudo apt update

RUN rm -f /etc/apt/apt.conf.d/docker-clean

# Add the Mono repository
#
ENV mono_repo_info="deb [signed-by=/usr/share/keyrings/mono-official-archive-keyring.gpg] https://download.mono-project.com/repo/debian stable-buster main"
#
RUN apt update \
    && apt install -y --no-install-recommends \
        dirmngr ca-certificates gnupg \
    && gpg --homedir /tmp --no-default-keyring \
        --keyring /usr/share/keyrings/mono-official-archive-keyring.gpg \
        --keyserver hkp://keyserver.ubuntu.com:80 \
        --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
    && echo "${mono_repo_info}" | tee /etc/apt/sources.list.d/mono-official-stable.list \
    && apt update \
    && apt-get purge -y --auto-remove gnupg dirmngr

# install dependencies
RUN  apt install -y --no-install-recommends \
        mono-devel \
    && rm -rf /var/lib/apt/lists/*

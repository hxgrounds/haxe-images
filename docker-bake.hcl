variable "LIX_VERSION" { default = "15.12.0"}
variable "HAXE_VERSION"{ default = "4.3.2"}
variable "org_name"    { default = "hxgrounds"}

variable "lix_image"    { default = "${org_name}/lix"}
variable "haxe_image"   { default = "${org_name}/lix-haxe"}
variable "haxe_cache_image" { default = "${org_name}/lix-haxe-cache"}
variable "python_image" { default = "${org_name}/haxe-python"}
variable "nodejs_image" { default = "${org_name}/haxe-nodejs"}
variable "java_image"   { default = "${org_name}/haxe-java"}
variable "php_image"    { default = "${org_name}/haxe-php"}

variable "csharp_image" { default = "${org_name}/haxe-csharp"}
variable "mono_image"   { default = "${org_name}/mono"}
variable "dotnet_sdk_image" { default = "mcr.microsoft.com/dotnet/sdk" }

variable "cpp_image"         { default = "${org_name}/haxe-cpp"}
variable "cpp_builder_image" { default = "${org_name}/cpp"}

variable "hashlink_image"  { default = "${org_name}/haxe-hashlink"}
variable "hashlink_base"   { default = "${org_name}/hashlink"}
variable "HASHLINK_VERSION"{ default = "1.13"}

variable "lua_image"       { default = "${org_name}/haxe-lua"}
variable "lua_base_image"  { default = "${org_name}/lua-dependencies"}
variable "LUA_VERSION"     { default = "5.4.6"}

variable "debian_default"  { default = "bookworm-slim"}
variable "debian_distros"  { default = ["bullseye-slim", "bookworm-slim"]}
variable "distro_variants" { default = concat([""], debian_distros) }

group "default" {
    targets = ["lix-haxe"]
}

group "all" {
    targets = ["lix", "lix-haxe", "python"]
}

group "plain" {
    targets = ["lix", "lix-haxe"]
}

group "hashlink-all" {
    targets = ["hashlink_base", "hashlink"]
}



// base images ------------------------------------------

variable "haxe_variants" { default = [
        for v in ["", HAXE_VERSION, "none"]: v == "none" ?
            { version: "", name: v}
            : { version: maybe(v, HAXE_VERSION), name : v}
    ] }

target "haxe-cache" {
    name        = targetName("haxe-cache", distro.name, haxe.name)
    dockerfile  = "dockerfiles/lix-haxe.Dockerfile"

    tags       = variantTags(haxe_cache_image, maybeJoin(distro.name, haxe.name), haxe.version )
    cache-from = [variantTags(haxe_cache_image, maybeJoin(distro.name, haxe.name), haxe.version)[0]]
    args = {
        HAXE_VERSION = haxe.version
        BASE = baseImage(distro.image, [distro.version, debian_default])
        KEEP_DOWNLOADS=1
    }
    matrix = {
        haxe = haxe_variants
        distro = concat(
            [for d in distro_variants: { image: "debian", version: d, name: d }],
            [{ image: "ubuntu", version: "jammy", name: "ubuntu-jammy"}]
        )
    }
}

target "haxe-variant" {
    name        = targetName("haxe-variant", distro, haxe_version)
    dockerfile  = "dockerfiles/haxe-variant.Dockerfile"
    args = {
        HAXE_IMG = variantImage(haxe_cache_image, distro, haxe_version)
        BASE     = baseImage("debian", [distro, debian_default])
    }
    matrix = {
        haxe_version = ["", "none"]
        distro       = distro_variants
    }
}

// ------------------------------------------

target "lix" {
    inherits   = [targetName("haxe-variant", distro, "none")]
    name       = targetName("lix", distro)
    tags       = variantTags(lix_image, distro, LIX_VERSION)
    cache-from = variantTags(lix_image, distro, LIX_VERSION)
    matrix = {
        distro = distro_variants
    }
}

target "lix-haxe" {
    inherits   = [targetName("haxe-variant", distro)]
    name       = targetName("lix-haxe", distro)
    tags       = variantTags(haxe_image, distro, HAXE_VERSION)
    cache-from = variantTags(haxe_image, distro, HAXE_VERSION)
    matrix = {
        distro = distro_variants
    }
}

target "python"{
    inherits = [targetName("haxe-variant", distro.parent)]
    name     = targetName("python", py_version, distro.name)
    args = {
        BASE = baseImage("python", [py_version, "3.12"], [distro.name, "slim-bookworm"])
    }
    tags       = [variantImage(python_image, py_version, distro.name)]
    cache-from = [variantImage(python_image, py_version, distro.name)]
    matrix = {
        distro     = [
            { name: "", parent:""},
            { name: "slim-bookworm", parent:"bookworm-slim"},
            { name: "slim-bullseye", parent:"bullseye-slim"},
        ]
        py_version = ["", "3", "3.12", "3.11", "3.10", "3.9", "3.8"]
    }
}


target "nodejs"{
    inherits = [targetName("haxe-variant", distro)]
    name     = targetName("nodejs", version, distro)
    args = {
        BASE = baseImage("node", [version, "lts"], [distro, "bookworm-slim"])
    }
    tags       = [variantImage(nodejs_image, version, distro)]
    cache-from = [variantImage(nodejs_image, version, distro)]
    matrix = {
        distro  = ["", "bullseye-slim", "bookworm-slim"]
        version = ["", "lts", "20", "18", "16"]
    }
}

target "java"{
    inherits = [targetName("haxe-variant")]
    name = targetName("java", version, distro)
    args = {
        HAXE_IMG = variantImage(haxe_cache_image, "ubuntu-jammy")
        BASE = baseImage("eclipse-temurin", [version, 21], ["jdk"], [distro, "jammy"])
    }
    tags       = [variantImage(java_image, version, distro)]
    cache-from = [variantImage(java_image, version, distro)]
    matrix = {
        distro  = [""]
        version = ["", "21", "17", "11", "8"]
    }
}


group "csharp" {
    targets = ["csharp-dotnet", "csharp-mono"]
}

target "csharp-dotnet"{
    inherits = [targetName("haxe-variant", maybe(distro, debian_default))]
    name     = targetName("csharp", "dotnet", sdk.version, distro)
    args = {
        BASE = baseImage(dotnet_sdk_image, [sdk.tag], [distro, debian_default])
    }
    tags       = [variantImage(csharp_image, "dotnet", sdk.version, distro)]
    cache-from = [variantImage(csharp_image, "dotnet", sdk.version, distro)]
    matrix = {
        distro  = ["", debian_default]
        sdk = [
            { tag: "8.0", version: ""},
            { tag: "8.0", version: "8"},
            { tag: "7.0", version: "7"},
            { tag: "6.0", version: "6"}
        ]
    }
}

target "csharp-mono"{
    inherits = [targetName("haxe-variant")]
    name = targetName("csharp", "mono", distro)
    contexts = {
        base = format("target:%s", targetName("mono", maybe(distro, debian_default)))
    }
    tags       = [variantImage(csharp_image, "mono", distro)]
    cache-from = [variantImage(csharp_image, "mono", distro)]
    matrix = {
        distro = distro_variants
    }
}

target "mono" {
    dockerfile = "dockerfiles/mono.Dockerfile"
    name       = targetName("mono", distro)
    tags       = [variantImage(mono_image, distro)]
    cache-from = [variantImage(mono_image, distro)]
    args = {
        BASE = baseImage("debian", [distro, debian_default])
    }
    matrix = {
        distro = distro_variants
    }
}

target "php"{
    inherits = [targetName("haxe-variant", variant.parent)]
    name     = targetName("php", variant.version, variant.distro)
    args = {
        BASE = baseImage("php", [variant.version, "8"], ["cli"], [variant.image, "bookworm"])
    }
    tags       = [variantImage(php_image, variant.version, variant.distro)]
    cache-from = [variantImage(php_image, variant.version, variant.distro)]
    matrix = {
        variant  = flatten([
            for version in ["", "8", "7"]: [
                for distro in ["", "bullseye", "bookworm"]:
                    {
                        parent: (distro == "") ? "" : ( (version != "7") ? "${distro}-slim" : "bullseye-slim" ),
                        image: version == "7" ? "bullseye" : distro,
                        distro: distro,
                        version: version
                    }
                if !(version == "7" && distro == "bookworm")
            ]
        ])
    }
}

target "cpp"{
    inherits = [targetName("haxe-variant")]
    name = targetName("cpp", distro)
    contexts = {
        base = format("target:%s", targetName("cpp-builder", maybe(distro, debian_default)))
    }
    tags       = [variantImage(cpp_image, distro)]
    cache-from = [variantImage(cpp_image, distro)]
    matrix = {
        distro = distro_variants
    }
}

target "cpp-builder" {
    dockerfile = "dockerfiles/gcc.Dockerfile"
    name       = targetName("cpp-builder", distro)
    tags       = [variantImage(cpp_builder_image, "gcc", distro)]
    cache-from = [variantImage(cpp_builder_image, "gcc", distro)]
    args = {
        BASE = baseImage("debian", [distro, debian_default])
    }
    matrix = {
        distro = distro_variants
    }
}

target "hashlink_base" {
    dockerfile = "dockerfiles/hashlink.Dockerfile"
    name       = targetName("hashlink_base", version)
    tags       = [variantImage(hashlink_base, version)]
    cache-from = [variantImage(hashlink_base, version)]
    args = {
        hashlink_version = maybe(version, HASHLINK_VERSION)
    }
    matrix = {
        version = ["", HASHLINK_VERSION]
    }
}

target "hashlink"{
    inherits = [targetName("haxe-variant")]
    name = targetName("hashlink", version)
    contexts = {
        base = "target:${targetName("hashlink_base", maybe(version, HASHLINK_VERSION))}"
    }
    tags       = [variantImage(hashlink_image, version)]
    cache-from = [variantImage(hashlink_image, version)]
    matrix = {
        version = ["", HASHLINK_VERSION]
    }
}


target "lua"{
    inherits = [targetName("haxe-variant", (distro == "" ? "" : "${distro}-slim"))]
    name = targetName("lua", distro, version)
    args = {
        BASE = baseImage(lua_base_image, [version, LUA_VERSION], [distro, "bookworm"])
    }
    tags       = versionImageTags(lua_image, version, distro)
    cache-from = versionImageTags(lua_image, version, distro)
    matrix = {
        distro  = ["", "bookworm"],
        version = ["", LUA_VERSION]
    }
}

target "lua-base" {
    dockerfile = "dockerfiles/lua-base.Dockerfile"
    name       = targetName("lua-base", distro, version)
    tags       = versionImageTags(lua_base_image, version, distro)
    cache-from = versionImageTags(lua_base_image, version, distro)
    args = {
        BASE = baseImage("nickblah/lua", [version, LUA_VERSION], ["luarocks"], [distro, "bookworm"])
    }
    matrix = {
        distro  = ["", "bookworm"],
        version = ["", LUA_VERSION]
    }
}

// Helpers ---------------------------------------------------------------

function "targetName" {
    params = [image]
    variadic_params = parts
    result = toName("${image}-${variantListTag(parts)}")
}


function "baseImage" {
    params = [image]
    variadic_params = paramPairs
    result = variantImageList(image,
                [for p in paramPairs: length(p) == 1 ? p[0] : maybe(p[0], p[1])])
}

function "maybe"{
    params = [value, default_value]
    result = (value == "" || value == null) ? default_value : value
}

function "toName" {
    params = [str]
    result = replace(str, ".", "_")
}


function "filterVersion" {
    params = [version]
    result = maybeRegex("v?([0-9]+([.][0-9]+)*(-[A-z0-9]+)?)", version)
}

function "maybeRegex" {
    params = [pattern, text]
    result = can(regex(pattern, "${text}")) ? "${regex(pattern, "${text}")[0]}" : ""
}

// Tag helpers  ---------------------------------------------

function "genTag" {
    params = [image_name, tagname]
    result = "${image_name}:${tagname}"
}

function "genTags" {
    params = [image_name, version]
    result = variantTags(image_name, "", version)
}

// TO-DO: maybe replace usages with versionImageTags using a default case in matrix params
function "variantTags" {
    params = [image_name, variant, version]
    result = concat(
        [
            variantImage(image_name, variant)
        ],
        version == "" ? [] : versionTags(image_name, version, variant)
    )
}

function "versionImageTags" {
    params = [image_name, version]
    variadic_params = params
    result = [
        for i, v in split(".", version):
            variantImageList(image_name, concat([parts(version, ".", i)], params))
    ]
}

function "versionTags" {
    params = [image_name, version, variant]
    result = [
        for i, v in split(".", version):
            maybeJoin(genTag(image_name, parts(version, ".", i)), variant)
    ]
}

// Variant images and tags ---------------------------------------------

function "variantImage" {
    params = [image_name]
    variadic_params = parts
    result = variantImageList(image_name, parts)
}
function "variantImageList" {
    params = [image_name, parts]
    result = "${image_name}:${variantListTag(parts)}"
}
function "variantTag" {
    params = []
    variadic_params = parts
    result = variantListTag(parts)
}
function "variantListTag" {
    params = [parts]
    result = maybeJoinList("-", [for p in parts: p if p != ""], "latest")
}
function "maybeJoinList" {
    params = [sep, parts, default_v]
    result = join("", parts) == "" ? default_v : join(sep, parts)
}


// ------------------------------------------------------------

function "parts"{
    params = [str, sep, idx]
    result = join(sep, slice(split(sep, str), 0, idx+1))
}

function "maybeJoin" {
    params = [part1, part2]
    result = (part1 == "" || part1 == null) ? part2 : (
                (part2 == "" || part2 == null) ? part1 : "${part1}-${part2}"
            )
}
# Haxe-Images

Docker images based on [haxe][haxe_org] and the [lix package manager][lix_pm].

[haxe_org]: https://haxe.org
[lix_pm]: https://github.com/lix-pm/lix.client

## How to Build

To build the images use [docker Bake](https://docs.docker.com/build/bake/):

`docker buildx bake [target]`

Choose one of the targets or groups at the hcl file.
For example:

* `all`: build all images
* `plain`: build `lix` and `lix_haxe` targets
* image targets: build respective images
    - `lix`
    - `lix_haxe`
    - `cpp`
    - `csharp`
    - `hashlink`
    - `java`
    - `nodejs`
    - `php`
    - `python`

## Supported images

* [lix][lix-image]: containing lix and nodejs, based on `debian:bullseye-slim` (by default).
* [haxe][haxe-image]: based on lix image, with haxe and neko downloaded.
* [haxe-python][hx-python-image]: includes a python interpreter with a default haxe version and lix with nodejs.
* [haxe-nodejs][hx-nodejs-image]: besides nodejs, includes also yarn and npm with a default haxe version and lix.
* [haxe-java][hx-java-image]: includes an openjdk version with a default haxe version and lix.
* [haxe-cpp][hx-cpp-image]: includes a c++ compiler (currently only gcc) with a default haxe version and lix.
* [haxe-csharp][hx-csharp-image]: includes a csharp compiler (either mono or dotnet SDK) with haxe and lix.
* [haxe-php][hx-php-image]: includes php-cli with a default haxe version and lix.
* [haxe-hashlink][hx-hl-image]: includes a hashlink compiler with a default haxe version and lix.

[lix-image]: https://hub.docker.com/r/hxgrounds/lix
[haxe-image]: https://hub.docker.com/r/hxgrounds/lix-haxe
[hx-python-image]: https://hub.docker.com/r/hxgrounds/haxe-python
[hx-nodejs-image]: https://hub.docker.com/r/hxgrounds/haxe-nodejs
[hx-java-image]: https://hub.docker.com/r/hxgrounds/haxe-java
[hx-cpp-image]: https://hub.docker.com/r/hxgrounds/haxe-cpp
[hx-csharp-image]: https://hub.docker.com/r/hxgrounds/haxe-csharp
[hx-php-image]: https://hub.docker.com/r/hxgrounds/haxe-php
[hx-hl-image]: https://hub.docker.com/r/hxgrounds/haxe-hashlink

### Tags

| Image |  tags | Base image |
| ----- | --------- | ----- |
| [hxgrounds/lix][lix-image]      | latest, 15, 15.12, 15.12.0, 15-bookworm-slim, 15.12-bookworm-slim, 15.12.0-bookworm-slim, bookworm-slim | debian:bookworm-slim |
| | 15-bullseye-slim, 15.12-bullseye-slim, 15.12.0-bullseye-slim, bullseye-slim | debian:bullseye-slim |
| [hxgrounds/lix-haxe][haxe-image] | latest, 4, 4.3, 4.3.2, 4-bookworm-slim, 4.3-bookworm-slim, 4.3.2-bookworm-slim, bookworm-slim | debian:bookworm-slim |
| | 4-bullseye-slim, 4.3-bullseye-slim, 4.3.2-bullseye-slim, bullseye-slim |debian:bullseye-slim |
| [hxgrounds/haxe-python][hx-python-image] | latest, 3, 3.12, 3.12-slim-bookworm, 3-slim-bookworm, slim-bookworm | python:3.12-slim-bookworm |
| | 3.12-slim-bullseye, 3-slim-bullseye, slim-bullseye | python:3.12-slim-bullseye |
| | 3.11, 3.11-slim-bookworm | python:3.11-slim-bookworm |
| | 3.11-slim-bullseye | python:3.11-slim-bullseye |
| | 3.10, 3.10-slim-bookworm | python:3.10-slim-bookworm |
| | 3.10-slim-bullseye | python:3.10-slim-bullseye |
| | 3.9, 3.9-slim-bookworm | python:3.9-slim-bookworm |
| | 3.9-slim-bullseye | python:3.9-slim-bullseye |
| | 3.8, 3.8-slim-bookworm | python:3.8-slim-bookworm |
| | 3.8-slim-bullseye | python:3.8-slim-bullseye |
| [hxgrounds/haxe-nodejs][hx-nodejs-image] | latest, lts, lts-bookworm-slim, bookworm-slim | node:lts-bookworm-slim |
| | lts-bullseye-slim, bullseye-slim | node:lts-bullseye-slim |
| | 20, 20-bookworm-slim | node:20-bookworm-slim |
| | 20-bullseye-slim | node:20-bullseye-slim |
| | 18, 18-bookworm-slim | node:18-bookworm-slim |
| | 18-bullseye-slim | node:18-bullseye-slim |
| | 16, 16-bookworm-slim | node:16-bookworm-slim |
| | 16-bullseye-slim | node:16-bullseye-slim |
| [hxgrounds/haxe-java][hx-java-image] | latest, 21 | eclipse-temurin:21-jdk-jammy |
| | 17 | eclipse-temurin:17-jdk-jammy |
| | 11 | eclipse-temurin:11-jdk-jammy |
| | 8  | eclipse-temurin:8-jdk-jammy |
| [hxgrounds/haxe-cpp][hx-cpp-image] | latest, bookworm-slim | debian:bookworm-slim |
| | bullseye-slim | debian:bullseye-slim |
| [hxgrounds/haxe-csharp][hx-cpp-csharp] | mono, mono-bookworm-slim | debian:bookworm-slim |
| | mono-bullseye-slim | debian:bullseye-slim |
| | dotnet, dotnet-bookworm-slim, dotnet-8, dotnet-8-bookworm-slim | debian:bookworm-slim |
| | dotnet-6, dotnet-6-bookworm-slim | debian:bookworm-slim |
| | dotnet-7, dotnet-7-bookworm-slim | debian:bookworm-slim |
| [hxgrounds/haxe-php][hx-php-image] | latest, 8, 8-bookworm, bookworm | php:8-cli-bookworm |
| | 8-bullseye, bullseye | php:8-cli-bullseye |
| | 7, 7-bullseye | php:7-cli-bullseye |
| [hxgrounds/haxe-hashlink][hx-hl-image] | latest, 1.13 | debian:bookworm-slim |

### Supported distributions

Currently, builds of most targets supports by default debian `bookworm-slim` and `bullseye-slim` versions, from [debian official docker image][docker-debian].

Java, however, is based on [eclipse-temurin images][eclipse-temurin-images],
which is one of the official jdk images on docker hub.
Eclipse-temurin [does not support debian][temurin-no-debian-issue],
so we use the default ubuntu jammy jellyfish as base for java images.

For php 7 there are no default images using debian bookworm, so only bullseye is supported.
Also, for hashlink and csharp:dotnet we are building only for bookworm.

We would like to support alpine builds too, however lix seems to not work with alpine for now.

[docker-debian]: https://hub.docker.com/_/debian
[gcc-docker]: https://hub.docker.com/_/gcc
[eclipse-temurin-images]: https://hub.docker.com/_/eclipse-temurin
[temurin-no-debian-issue]: https://github.com/adoptium/containers/issues/99

## License

This project is licensed under the MIT [non-AI license](https://github.com/non-ai-licenses/non-ai-licenses). See [LICENSE.md](./LICENSE.md) file.

So the content is this repository is not to be used on AI training datasets or technologies without permission.
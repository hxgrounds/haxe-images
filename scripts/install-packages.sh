# !/bin/sh

PACKAGES=$@

rm -f /etc/apt/apt.conf.d/docker-clean

set -x

apt-get update
apt-get install -y --no-install-recommends $PACKAGES

# rm -rf /var/lib/apt/lists/*
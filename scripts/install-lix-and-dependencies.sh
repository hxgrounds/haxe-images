#!/bin/sh

# 0. Functions -----------------------------------------------------

has_command() {
    command -v $1 > /dev/null
}

append_to_install() {
    if ! has_command $1; then
        echo "$2 $1"
    else
        echo "$2"
    fi
}

node_installer() {
    if has_command yarn; then
        echo "yarn global add"
    elif has_command yarnpkg; then
        echo "yarnpkg global add"
    elif has_command npm; then
        echo "npm install -g"
    fi
}

# 1. Setup: detect required packages -------------------------------

packages="ca-certificates"
packages=$(append_to_install zip "$packages")
packages=$(append_to_install git "$packages")
packages=$(append_to_install nodejs "$packages")

temp_packages=""

# Test if output of node_installer is empty
if [ -z "$(node_installer)" ] && [ -z "$NO_LIX" ]; then
    temp_packages="yarnpkg"
    packages="$packages $temp_packages"
fi

# 2. Installing ----------------------------------------------------

## 2.a. Installing packages ------------------
#
if ! [ -z "$packages" ]; then
    echo "installing $packages"

    if [ -z "$NO_UPDATE" ]; then
        apt update
    fi

    options=""

    if [ -z "$KEEP_DOWNLOADS" ]; then
        options='-o APT::Keep-Downloaded-Packages="true"'
    fi
    apt-get install $packages -y --no-install-recommends $options
fi

## 2.b. Installing lix -----------------------
#
if [ -z "$NO_LIX" ]; then
    echo "Installing lix"
    $(node_installer) lix
fi

# 3. teardown: removing unneeded packages and files ----------------
#
if ! [ -z "$temp_packages" ]; then
    echo "removing $temp_packages"
    apt-get purge -y --auto-remove $temp_packages
fi

if [ -n "$packages" ] && [ -z "$KEEP_DOWNLOADS" ]; then
    rm -rf /var/lib/apt/lists/*
fi

# ------------------------------------------------------------------